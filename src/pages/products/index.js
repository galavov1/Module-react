import './products.css'
import Card from '../../components/elements/card/card.js';

function Products() {
    return (
        <div className="products">
            <div className="products__header">
                <h1 className='title'>наша продукция</h1>
                <div className="products__calc">
                    <div className="products__list">
                        <p>3 товара</p>
                        <p>на сумму 3 500 ₽</p>
                    </div>
                    <button className="products__button"></button>
                </div>
            </div>
            <div className="products__wrapper">
                <Card
                url='./images/1.png'
                title='Устрицы по рокфеллеровски'
                description='Устрицы запеченные в половинках панциря с густым соусом из сливочного масла, трав и панировочных сухарей'
                price='2 700 ₽'
                quantity='/ 500 г.'
                />
                <Card
                url='./images/2.png'
                title='Свиные ребрышки на гриле с зеленью'
                description='Свиные ребрышки в медовой глазури, обжаренные на открытом огне, подаются с острым соусом'
                price='1 600 ₽'
                quantity='/ 750 г.'
                />
                <Card
                url='./images/3.png'
                title='Креветки по-королевски
                в лимонном соке'
                description='Тихоокеанские креветки приготовленные на гриле с лимонои и травами'
                price='1820 ₽'
                quantity='/ 350 г.'
                />
                <Card
                url='./images/1.png'
                title='Устрицы по рокфеллеровски'
                description='Устрицы запеченные в половинках панциря с густым соусом из сливочного масла, трав и панировочных сухарей'
                price='2 700 ₽'
                quantity='/ 500 г.'
                />
                <Card
                url='./images/2.png'
                title='Свиные ребрышки на гриле с зеленью'
                description='Свиные ребрышки в медовой глазури, обжаренные на открытом огне, подаются с острым соусом'
                price='1 600 ₽'
                quantity='/ 750 г.'
                />
                <Card
                url='./images/3.png'
                title='Креветки по-королевски
                в лимонном соке'
                description='Тихоокеанские креветки приготовленные на гриле с лимонои и травами'
                price='1820 ₽'
                quantity='/ 350 г.'
                />
                <Card
                url='./images/1.png'
                title='Устрицы по рокфеллеровски'
                description='Устрицы запеченные в половинках панциря с густым соусом из сливочного масла, трав и панировочных сухарей'
                price='2 700 ₽'
                quantity='/ 500 г.'
                />
                <Card
                url='./images/2.png'
                title='Свиные ребрышки на гриле с зеленью'
                description='Свиные ребрышки в медовой глазури, обжаренные на открытом огне, подаются с острым соусом'
                price='1 600 ₽'
                quantity='/ 750 г.'
                />
            </div>

        </div>
    );
}

export default Products;
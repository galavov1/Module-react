import styles from './card.module.css'

function Card({url, title, description, price, quantity}) {
    const handleClick = () => {
        console.log('title');
    }
    return (
        <div className={styles.card}>
            <img className={styles.card__preview} src={url} alt={title} />
            <h3 className={styles.card__title}>{title}</h3>
            <p className={styles.card__description}>{description}</p>

            <div className={styles.card__footer}>
                <p className={styles.card__price}>{price} <span className={styles.card__quantity}>{quantity}</span></p>
                <button className={styles.card__button} onClick={handleClick}></button>
            </div>
        </div>
    );
}

export default Card;